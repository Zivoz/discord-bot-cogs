import io
import os.path
from cogs.utils.dataIO import dataIO
import aiohttp
import aiohttp
from .utils.dataIO import fileIO
import discord
from discord.ext import commands
from random import randrange, uniform
import glob, random
import sys
import os
import datetime
try: # check if BeautifulSoup4 is installed
	from bs4 import BeautifulSoup
	soupAvailable = True
except:
	soupAvailable = False
class ACZEROTwitchEmotesModule:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, no_pm=True, aliases=['Fancymention'])
    async def fancymention(self, ctx, user : discord.Member):
            ez = [open(file, 'r').read() for file in glob.glob('data/emote/*.json')]

            await self.bot.say(
                "{e} {author_mention} would like your attention {user_mention}!!!".format(
                author_mention=ctx.message.author.mention,
                user_mention=user.mention,
                e=ez[randrange(len(ez))]
        )
)


    @commands.command(pass_context=True, no_pm=True)
    async def superadd(self, ctx, song_url):
        """
        Usage:
        {command_prefix}add song url
        adds song to the autoplaylist
        *This is only usable by specific people*
        """
        song_url2 = str(song_url)
        name = str(ctx.message.author.name)
        url = song_url2
        idx = str(ctx.message.author.mention)
        if "<@!95208204740341760>" in idx or "<@!148943582248566784>" in idx:
            if ("youtu" or "youtube") not in song_url2 or "repeat" in song_url2:
                await self.bot.say("This isn't a youtube link.")
                return
            async with aiohttp.get(url) as response:
                soupObject = BeautifulSoup(await response.text(), "html.parser")
                try:
                    song_name = soupObject.find(class_='watch-title').get_text().rstrip().lstrip()
                except:
                    await self.bot.say("could not find song name")
                    return
                if "&" in song_url2:
                    sep = '&'
                    song_url2 = song_url2.split(sep, 1)[0]
                    await self.bot.say("converted playlist url to normal url " + "```" + song_url2 + "```")
                if song_url2 in open('/home/bot/musicbot/config/autoplaylist.txt').read():
                    await self.bot.say("this song is already on the autoplaylist.")
                    return
                else:
                    with open("/home/bot/musicbot/config/autoplaylist.txt", "a") as myfile:
                        myfile.write(song_url2 + "\n")
                        await self.bot.say("You have added '" + song_name + "' to the autoplaylist.")
                    with open("/home/bot/musicbot/config/culprit.txt", "a") as myfile:
                        myfile.write(song_url2 + " " + name + " - " + song_name + "\n")
        else:
            await self.bot.say("You do not have permission to use this command.")
    @commands.command(pass_context=True, no_pm=True)
    async def add(self, ctx, song_url):
        """
        Usage:
        {command_prefix}add song url
        adds song to the autoplaylist
        """
        song_url2 = str(song_url)
        name = str(ctx.message.author.name)
        url = song_url2
        if ("youtu" or "youtube") not in song_url2 or "repeat" in song_url2:
            await self.bot.say("This isn't a youtube link.")
            return
        async with aiohttp.get(url) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                song_name = soupObject.find(class_='watch-title').get_text().rstrip().lstrip()
            except:
                await self.bot.say("could not find song name")
                return
            if "loli" in song_name.lower().strip() or "asmr" in song_name.lower().strip() or "loud" in song_name.lower().strip() or "horn" in song_name.lower().strip() or "earrape" in song_name.lower().strip():
                await self.bot.say("hey... " + name + ", contact <@148943582248566784>")
                return
            if "&" in song_url2:
                sep = '&'
                song_url2 = song_url2.split(sep, 1)[0]
                await self.bot.say("converted playlist url to normal url " + "```" + song_url2 + "```")
            if song_url2 in open('/home/bot/musicbot/config/autoplaylist.txt').read():
                await self.bot.say("this song is already on the autoplaylist.")
                return
            else:
                with open("/home/bot/musicbot/config/autoplaylist.txt", "a") as myfile:
                    myfile.write(song_url2 + "\n")
                    await self.bot.say("You have added '" + song_name + "' to the autoplaylist.")
                with open("/home/bot/musicbot/config/culprit.txt", "a") as myfile:
                    myfile.write(song_url2 + " " + name + " - " + song_name + "\n")

    @commands.command(pass_context=True, no_pm=True, aliases=['punch', 'Hit', 'Punch'])
    async def hit(self, ctx, user : discord.Member):
        user1 = user.mention .format(str);
        if user1 == "<@!148943582248566784>":
            await self.bot.say("I will not hit my master")
        elif user1 == "<>":
            await self.bot.say(ctx.message.author.mention + " hit himself in confusion")
        elif user1 == "<@!95208204740341760>":
            await self.bot.say("That would be animal abuse.")
        else:
            await self.bot.say("One hit and " + user.mention + " is out! ᕙ༼ຈل͜ຈ༽ᕗ")

    @commands.command(pass_context=True, no_pm=True, aliases=['Gnome_child', 'Gnome', 'gnome'])
    async def Gnome_Child(self, ctx):
        await self.bot.type()
        url = "http://i.imgur.com/0A7VaMr.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)

    @commands.command(pass_context=True, no_pm=True, aliases=['hahaa'])
    async def haHAA(self, ctx):
        await self.bot.type()
        url = "https://cdn.discordapp.com/attachments/302818934493806603/336588065457963009/haHAA_HD.jpg"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)

    @commands.command(pass_context=True, no_pm=True, aliases=['Kreygasm', 'kreygasm'])
    async def KreyGasm(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/UtgMN8M.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['kappa'])
    async def Kappa(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/NJj2WOy.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['4head', '4Head'])
    async def FourHead(self, ctx):

        await self.bot.type()

        url = "http://i.imgur.com/aWpCw3S.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['dansgame', 'Dansgame'])
    async def DansGame(self, ctx):

        await self.bot.type()

        url = "http://i.imgur.com/GJoObVC.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['babyrage', 'Babyrage'])
    async def BabyRage(self, ctx):

        await self.bot.type()

        url = "http://i.imgur.com/7in6UqH.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['coolstorybob', 'Coolstorybob'])
    async def CoolStoryBob(self, ctx):

        await self.bot.type()

        url = "http://i.imgur.com/w8rMixV.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['Brokeback', 'brokeback'])
    async def BrokeBack(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/fUrKwE0.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)

    @commands.command(pass_context=True, no_pm=True, aliases=['cheffrank', 'Cheffrank'])
    async def ChefFrank(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/EiDnFaH.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['Elegiggle', 'elegiggle'])
    async def EleGiggle(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/QkuX5XD.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['Failfish', 'failfish'])
    async def FailFish(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/EHEniHO.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True)
    async def REEEEEEEE(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/TzFAQTU.gif"
        filename = "anything.gif"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True)
    async def REEE(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/7l09D8Y.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['cory'])
    async def Cory(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/EX2WDt1.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['Vohiyo', 'vohiyo'])
    async def VoHiYo(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/xwZoRlV.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['smorc', 'Smorc'])
    async def SMOrc(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/dZBCYRr.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['seemsgood', 'Seemsgood'])
    async def SeemsGood(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/2gYUYSB.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['Residentsleeper', 'residentsleeper'])
    async def ResidentSleeper(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/UdXkwqf.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['pogchamp', 'Pogchamp'])
    async def PogChamp(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/TuouuFU.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command()
    async def Scamaz(self):
        await self.bot.type()
        url = "http://i.imgur.com/A3kmHUS.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['wutface', 'Wutface'])
    async def WutFace(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/1JdLiGS.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['feelsamazingman', 'Feelsamazingman'])
    async def FeelsAmazingMan(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/PoDiwnp.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['feelsgoodman', 'Feelsgoodman'])
    async def FeelsGoodMan(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/dLMH4bR.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)
    @commands.command(pass_context=True, no_pm=True, aliases=['feelsbadman', 'Feelsbadman'])
    async def FeelsBadMan(self, ctx):
        await self.bot.type()

        url = "http://i.imgur.com/aXMjba4.png"
        filename = "anything.png"
        async with aiohttp.get(url) as image:
            await self.bot.upload(
                io.BytesIO(await image.read()), filename=filename, content=ctx.message.author.mention)




def setup(bot):
    bot.add_cog(ACZEROTwitchEmotesModule(bot))
