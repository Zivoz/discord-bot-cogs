import discord
from discord.ext import commands
from .utils.dataIO import fileIO
import os
import os.path
from cogs.utils.dataIO import dataIO
import asyncio
import time
import logging
import re
import aiohttp
from bs4 import BeautifulSoup

class ModCogs:
    """Several commands that make moderation easier."""

    def __init__(self, bot):
        self.bot = bot
        self.members = fileIO("data/modcog/clanmembers.json", "load")
        self.ehp = {450, 600, 900,1200, 1600}
        self.total = {1850, 2000, 2100, 2200, 2277}
        self.rank = ["smiley", "recruit", "corporal", "sergeant", "lieutenant", "captain"]

    @commands.command(aliases=["modcommands", "modhelp", "modcog", "modcogs"])
    async def modcomms(self):
        embed=discord.Embed(title="", color=0x0000a0)
        embed.set_author(name="Made by Azero",icon_url="http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/9e/9ec330a25945e0c62618dbc2ab282978bd1f4aef_full.jpg")
        embed.add_field(name="%addmember", value="Adds a member to the member list.", inline=False)
        embed.add_field(name="%allmembers", value="Shows all added clan members.", inline=False)
        embed.add_field(name="%memberstats", value="Shows basic stats about the clan members.", inline=False)
        embed.add_field(name="%member", value="Shows stats for a certain member.", inline=False)
        embed.add_field(name="%removemember", value="Removes a member from the files", inline=False)
        embed.add_field(name="%updatemembers", value="Updates EHP and Total level for all members. For debug add 1 behind command.", inline=False)
        embed.add_field(name="%memberehp", value="Show the amount of member above and below a certain EHP. For debug add 1 behind command.", inline=False)
        embed.add_field(name="%membertotal", value="Show the amount of member above and below a certain Total. For debug add 1 behind command.", inline=False)
        embed.add_field(name="%memberranks", value="Shows the amount of people per rank. For debug add 1 behind command.", inline=False)
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True, aliases=["addMember", "Addmember", "AddMember"])
    async def addmember(self, ctx, *, username):
        """Adds a member to the member list
        Example:
        [p]addmember OSRS_NAME"""
        author = ctx.message.author

        admin_role = False

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            s = re.sub('\s+', '+', username)
            s = s.lower()
            xpURL = "http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1={0}".format(s)
            ehpURL = "https://crystalmathlabs.com/tracker/track.php?player={0}".format(s)

            async with aiohttp.get(ehpURL) as response:
                soupObject = BeautifulSoup(await response.text(), "html.parser")
                try:
                    ehp = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                    ehp = ehp.split(" ")
                    ehp = str(ehp[18])
                    ehp = ehp.replace("Week:", "")
                    ehp = ehp.replace("First", "")
                    ehp = ehp.replace(",", "")

                    ehp_counter = 0

                    for value in self.ehp:
                        if float(ehp) > float(value):
                            ehp_counter = ehp_counter + 1

                    async with aiohttp.get(xpURL) as response:

                        soupObject2 = BeautifulSoup(await response.text(), "html.parser")

                        table_data = soupObject2.find(id='contentHiscores').find_all('td')
                        total_level = str(table_data[13])
                        total_level = re.split(r'[><]', total_level)
                        total = total_level[2].replace(",", "")

                        total_counter = 0

                        for value in self.total:
                            if float(total) > float(value):
                                total_counter = total_counter + 1

                    if ehp_counter > total_counter:
                        member_rank = self.rank[ehp_counter]
                    else:
                        member_rank = self.rank[total_counter]


                    user_already_added = False

                    for entry in self.members:
                        if s in entry["NAME"]:
                            user_already_added = True

                    if user_already_added == True:
                        await self.bot.say("That member is already added to the list")
                    else:
                        await self.bot.say("Member: {} added.".format(s) + "\n" + "His EHP is: {} and his total level is: {}.".format(ehp,total) + "\n" + "His rank is {}.".format(member_rank))
                        self.members.append({"NAME": s, "EHP": ehp, "TOTAL": total})
                        logger.info("{} ({}) added {} to the member list.".format(author.name, author.id, s))
                        fileIO("data/modcog/clanmembers.json", "save", self.members)

                except BaseException as e:
                    await self.bot.say("Adding member failed: " + str(e) + "\n" + "Maybe you typed the wrong username?" + "\n" + "If not: Contact Azero or TypeO to fix your shit")
        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")

    @commands.command(pass_context=True, aliases=["Allmembers", "allMembers", "AllMembers", "AM"])
    async def allmembers(self, ctx):
        """Prints all members"""

        members = []
        total_members = 0
        admin_role = False

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            for entry in self.members:
                members.append(entry["NAME"])
                total_members = total_members + 1

            await self.bot.say("The total amount of members is " + str(total_members) + "\n" + "The members are:")
            await self.bot.say(members)

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")

    @commands.command(pass_context=True, aliases=["Memberstats", "memberStats", "MemberStats", "MS"])
    async def memberstats(self, ctx):
        """Prints basic info about the members"""

        total_members = 0
        average_ehp = 0
        average_total = 0
        admin_role = False

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            for entry in self.members:
                average_ehp = average_ehp + float(entry["EHP"])
                average_total = average_total + float(entry["TOTAL"])
                total_members = total_members + 1

            average_total = average_total / total_members
            average_total = int(average_total)
            average_ehp = average_ehp / total_members
            average_ehp = int(average_ehp)

            await self.bot.say("The total amount of members is " + str(total_members) + "\n" + "The average EHP is " + str(average_ehp) + "\n" + "The average Total is " + str(average_total))

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")

    @commands.command(pass_context=True, aliases=["Member", "MEMBER"])
    async def member(self, ctx, *, username):
        """Prints info of a member
            Example:
            %member Azero"""

        admin_role = False
        player_found = False

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            s = re.sub('\s+', '+', username)
            s = s.lower()
            for entry in self.members:
                if str(s) == entry["NAME"]:
                    player_found = True;
                    await self.bot.say("Stats of player '" + entry["NAME"] + "' are:" + "\n" + "Total " + entry["TOTAL"] + "\n" + "EHP " + entry["EHP"])

            if player_found == False:
                await self.bot.say("Player not found.")

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")


    @commands.command(pass_context=True, aliases=["Removemember", "RemoveMember", "removeMember", "RM"])
    async def removemember(self, ctx, *, username):
        """Removes a member
            Example:
            %removemember Azero"""

        admin_role = False

        to_remove = []

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            s = re.sub('\s+', '+', username)
            s = s.lower()

            for entry in self.members:
                if entry["NAME"] == s:
                    to_remove.append(entry)

            if not to_remove == []:
                for entry in to_remove:
                    self.members.remove(entry)
                fileIO("data/modcog/clanmembers.json", "save", self.members)
                await self.bot.say("User '" + s + "' removed.")
            else:
                await self.bot.say("User not found.")

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")


    @commands.command(pass_context=True, aliases=["UM"])
    async def updatemembers(self, ctx, *args):
        """Updates member list"""
        admin_role = False

        debug = 0

        for arg in args:
            debug = 1

        new_ehp = 0
        new_total = 0

        total_succesfull_ehp = 0
        total_succesfull_total = 0

        total_failed_total = 0
        total_failed_ehp = 0

        succesfull_ehp = []
        succesfull_total = []

        failed_ehp = []
        failed_total = []

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:

            for entry in self.members:
                s = entry["NAME"]

                ehpupdateURL = "https://crystalmathlabs.com/tracker/update.php?player={0}".format(s)
                ehpURL = "https://crystalmathlabs.com/tracker/track.php?player={0}".format(s)
                xpURL = "http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1={0}".format(s)

                async with aiohttp.get(ehpupdateURL) as response:
                    try:
                        soupObject = BeautifulSoup(await response.text(), "html.parser")

                    except BaseException as e:
                        await self.bot.say("EHP update error: " + str(e))


                async with aiohttp.get(ehpURL) as response:
                    soupObject = BeautifulSoup(await response.text(), "html.parser")
                    try:
                        ehp = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                        ehp = ehp.split(" ")
                        ehp = str(ehp[18])
                        ehp = ehp.replace("Week:", "")
                        ehp = ehp.replace("First", "")
                        ehp = ehp.replace(",", "")

                        new_ehp = ehp

                    except BaseException as e:
                        await self.bot.say("EHP fetch error: " + str(e))

                async with aiohttp.get(xpURL) as response:
                    try:

                        soupObject = BeautifulSoup(await response.text(), "html.parser")

                        table_data = soupObject.find(id='contentHiscores').find_all('td')
                        total_level = str(table_data[13])
                        total_level = re.split(r'[><]', total_level)
                        total = total_level[2].replace(",", "")

                        new_total = total

                    except BaseException as e:
                        await self.bot.say("Total fetch error: " + str(e))

                if float(new_ehp) != float(entry["EHP"]):
                    succesfull_ehp.append(entry["NAME"])
                    total_succesfull_ehp = total_succesfull_ehp + 1

                    entry["EHP"] = new_ehp

                else:
                    failed_ehp.append(entry["NAME"])
                    total_failed_ehp = total_failed_ehp + 1


                if float(new_total) != float(entry["TOTAL"]):
                    succesfull_total.append(entry["NAME"])
                    total_succesfull_total = total_succesfull_total + 1

                    entry["TOTAL"] = new_total

                else:
                    failed_total.append(entry["NAME"])
                    total_failed_total = total_failed_total + 1

            fileIO("data/modcog/clanmembers.json", "save", self.members)

            await self.bot.say("The EHP for {} members is updated.".format(total_succesfull_ehp))
            if debug == 1:
                await self.bot.say(succesfull_ehp)
            await self.bot.say("The EHP for {} members is not updated.".format(total_failed_ehp))
            if debug == 1:
                await self.bot.say(failed_ehp)

            await self.bot.say("The Total for {} members is updated.".format(total_succesfull_total))
            if debug == 1:
                await self.bot.say(succesfull_total)
            await self.bot.say("The Total for {} members is not updated".format(total_failed_total))
            if debug == 1:
                await self.bot.say(failed_total)


        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")

    @commands.command(pass_context=True, aliases=["Memberehp", "MemberEhp", "MemberEHP", "ME"])
    async def memberehp(self, ctx, ehp, *args):
        """Prints the amount of people above and below a certain ehp
            Example:
            %memberehp 100"""

        debug = 0

        for arg in args:
            debug = 1


        total_above = 0
        total_below = 0

        above = []
        below = []

        admin_role = False

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            for entry in self.members:
                if float(entry["EHP"]) >= float(ehp):
                    total_above = total_above + 1
                    above.append(entry["NAME"])
                else:
                    total_below = total_below + 1
                    below.append(entry["NAME"])

            await self.bot.say("The amount of people above {} ehp is {}.".format(ehp,total_above))
            if debug == 1:
                await self.bot.say(above)
            await self.bot.say("The amount of people below {} ehp is {}.".format(ehp, total_below))
            if debug == 1:
                await self.bot.say(below)

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")


    @commands.command(pass_context=True, aliases=["MemberTotal", "Membertotal", "memberTotal", "MT"])
    async def membertotal(self, ctx, total, *args):
        """Prints the amount of people above and below a certain total
            Example:
            %membertotal 100"""

        debug = 0

        for arg in args:
            debug = 1


        total_above = 0
        total_below = 0

        above = []
        below = []

        admin_role = False

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            for entry in self.members:
                if float(entry["TOTAL"]) >= float(total):
                    total_above = total_above + 1
                    above.append(entry["NAME"])
                else:
                    total_below = total_below + 1
                    below.append(entry["NAME"])

            await self.bot.say("The amount of people above {} total is {}.".format(total,total_above))
            if debug == 1:
                await self.bot.say(above)
            await self.bot.say("The amount of people below {} total is {}.".format(total, total_below))
            if debug == 1:
                await self.bot.say(below)

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")

    @commands.command(pass_context=True, aliases=["MemberRanks", "memberRanks","Memberranks","MR"])
    async def memberranks(self, ctx, *args):
        """Prints the amount of members per rank"""

        debug = 0

        for arg in args:
            debug = 1

        admin_role = False

        ehp_counter = 0
        total_counter = 0

        rank_counter = 0

        smiley_counter = 0
        recruit_counter = 0
        corporal_counter = 0
        sergeant_counter = 0
        lieutenant_counter = 0
        captain_counter = 0

        smiley = []
        recruit = []
        corporal = []
        sergeant = []
        lieutenant = []
        captain = []

        for role in ctx.message.author.roles:
            if str(role) == "Admin":
                admin_role = True

        if admin_role == True:
            for entry in self.members:
                for ehp in self.ehp:
                    if float(entry["EHP"]) > ehp:
                        ehp_counter = ehp_counter + 1
                for total in self.total:
                    if float(entry["TOTAL"]) > total:
                        total_counter = total_counter + 1

                if total_counter >= ehp_counter:
                    rank_counter = total_counter
                else:
                    rank_counter = ehp_counter

                total_counter = 0
                ehp_counter = 0

                if self.rank[rank_counter] == "smiley":
                    smiley_counter = smiley_counter + 1
                    smiley.append(entry["NAME"])
                elif self.rank[rank_counter] == "recruit":
                    recruit_counter = recruit_counter + 1
                    recruit.append(entry["NAME"])
                elif self.rank[rank_counter] == "corporal":
                    corporal_counter = corporal_counter + 1
                    corporal.append(entry["NAME"])
                elif self.rank[rank_counter] == "sergeant":
                    sergeant_counter = sergeant_counter + 1
                    sergeant.append(entry["NAME"])
                elif self.rank[rank_counter] == "lieutenant":
                    lieutenant_counter = lieutenant_counter + 1
                    lieutenant.append(entry["NAME"])
                elif self.rank[rank_counter] == "captain":
                    captain_counter = captain_counter + 1
                    captain.append(entry["NAME"])


            await self.bot.say("There are {} smileys, {} recruits, {} corporals, {} lieutenants and {} captains.".format(smiley_counter,recruit_counter,corporal_counter,lieutenant_counter,captain_counter))

            if debug == 1:

                smiley_list = ""
                recruit_list = ""
                corporal_list = ""
                sergeant_list = ""
                lieutenant_list = ""
                captain_list = ""

                for rank in smiley:
                    smiley_list = smiley_list + "\n" + rank

                for rank in recruit:
                    recruit_list = recruit_list + "\n" + rank

                for rank in corporal:
                    corporal_list = corporal_list + "\n" + rank

                for rank in sergeant:
                    sergeant_list = sergeant_list + "\n" + rank

                for rank in lieutenant:
                    lieutenant_list = lieutenant_list + "\n" + rank

                for rank in captain:
                    captain_list = captain_list + "\n" + rank

                if smiley_counter == 0:
                    smiley_list = "-"

                if recruit_counter == 0:
                    recruit_list = "-"

                if corporal_counter == 0:
                    corporal_list = "-"

                if sergeant_counter == 0:
                    sergeant_list = "-"

                if lieutenant_counter == 0:
                    lieutenant_list = "-"

                if captain_counter == 0:
                    captain_list = "-"


                embed = discord.Embed(title="", color=0x0000a0)
                embed.set_author(name="Made by Azero", icon_url="http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/9e/9ec330a25945e0c62618dbc2ab282978bd1f4aef_full.jpg")
                embed.add_field(name="Smiley", value=smiley_list, inline=False)
                embed.add_field(name="Recruit", value=recruit_list, inline=False)
                embed.add_field(name="Corporal", value=corporal_list, inline=False)
                embed.add_field(name="Sergeant", value=sergeant_list, inline=False)
                embed.add_field(name="Lieutenant", value=lieutenant_list, inline=False)
                embed.add_field(name="Captain", value=captain_list, inline=False)

                await self.bot.say(embed=embed)

        else:
            await self.bot.say("Try that again and im going to leak your nudes " + str(ctx.message.author) + "!")

def check_folders():
    if not os.path.exists("data/modcog"):
        print("Creating data/modcog folder...")
        os.makedirs("data/modcog")


def check_files():
    f = "data/modcog/clanmembers.json"
    if not fileIO(f, "check"):
        print("Creating empty clanmembers.json...")
        fileIO(f, "save", [])


def setup(bot):
    global logger
    check_folders()
    check_files()
    logger = logging.getLogger("modcog")
    if logger.level == 0:  # Prevents the logger from being loaded again in case of module reload
        logger.setLevel(logging.INFO)
        handler = logging.FileHandler(filename='data/modcog/modcog.log', encoding='utf-8', mode='a')
        handler.setFormatter(logging.Formatter('%(asctime)s %(message)s', datefmt="[%d/%m/%Y %H:%M]"))
        logger.addHandler(handler)
    n = ModCogs(bot)
    # loop = asyncio.get_event_loop()
    # loop.create_task(n.check_reminders())
    bot.add_cog(n)
