import io
import binascii
import os.path
from cogs.utils.dataIO import dataIO
import aiohttp
import discord
from discord.ext import commands
from random import randrange, uniform
import glob, random
class randomimageModule:
	def __init__(self, bot):
		self.bot = bot

	@commands.command()
	async def randimage(self):
		random = binascii.b2a_hex(os.urandom(3)).decode("utf-8")
		await self.bot.say("https://prnt.sc/" + random)

	@commands.command()
	async def massrandimage(self):
		for x in range(4):
			random = binascii.b2a_hex(os.urandom(3)).decode("utf-8")
			await self.bot.say("https://prnt.sc/" + random)
			
def setup(bot):
	bot.add_cog(randomimageModule(bot))
