import io
import os.path
from cogs.utils.dataIO import dataIO
import aiohttp
from .utils.dataIO import fileIO
import discord
from random import choice as randchoice
import os
from discord.ext import commands
from random import randrange, uniform
import glob, random
import re
import string
from discord.ext import commands
from .utils.dataIO import dataIO
from .utils import checks
from __main__ import send_cmd_help, settings
from datetime import datetime
from collections import deque, defaultdict
from cogs.utils.chat_formatting import escape_mass_mentions, box
import asyncio
import urllib.request
from time import sleep
try: # check if BeautifulSoup4 is installed
	from bs4 import BeautifulSoup
	soupAvailable = True
except:
	soupAvailable = False
class ACZEROrsmodule:
    def __init__(self, bot):
        self.bot = bot
        self.wsids = fileIO("data/rscog/wsid.json","load")
    @commands.command(aliases=["runescape", "osrs", "rscogs", "rscommands"])
    async def rscomms(self):
        embed=discord.Embed(title="", color=0x0000a0)
        embed.set_author(name="Made by Azero",icon_url="http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/9e/9ec330a25945e0c62618dbc2ab282978bd1f4aef_full.jpg")
        embed.add_field(name="%gains", value="Check your current gains, you can use day/week/month/year/total :)", inline=False)
        embed.add_field(name="%max", value="A tool to check how much you've maxed your account", inline=False)
        embed.add_field(name="%update", value="Updates your CML page")
        embed.add_field(name="%whatdo", value="Bot comes up with something to do for you.", inline=False)
        embed.add_field(name="%ehp", value="Checks your EHP.", inline=False)
        embed.add_field(name="%best_day/week/month/year", value="Checks your best XP/EHP records.", inline=False)
        await self.bot.say(embed=embed)
    @commands.command(aliases=["07gains"])
    async def gains_outdated(self, *, name):
        "Put your osrs name here."
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/sig.php?name={0}" .format(s)
        url2 = "https://crystalmathlabs.com/tracker/update.php?player={0}" .format(s)
        filename = "gains.png"
        async with aiohttp.get(url2) as text:
            sleep(1)
            async with aiohttp.get(url) as image:
                await self.bot.upload(
                    io.BytesIO(await image.read()), filename=filename, content="This script auto updates every 30 seconds.")

    @commands.command(aliases=["Update"])
    async def update(self, *, name):
        "Put your osrs name here."
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/update.php?player={0}" .format(s)
        try:
            async with aiohttp.get(url) as response:
                soupObject = BeautifulSoup(await response.text(), "html.parser")
                found = soupObject.find(class_="explain")
                print(found)
                if found == None:
                    await self.bot.say("User was not found, or was updated less than 60 seconds ago.")
                    return
                embed=discord.Embed(title="", color=0x0000a0)
                embed.add_field(name=name, value="Your CML page has been successfully updated", inline=False)
                await self.bot.say(embed=embed)
        except BaseException as e:
            await self.bot.say("failed to update: " + str(e))

    @commands.command(aliases=["max"])
    async def tomax(self, *, name):
        "Put your osrs name here."
        s = re.sub('\s+', '+', name)
        url = "http://oldschool.runeclan.com/user/{0}/stats" .format(s)
        async with aiohttp.get(url) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                xp = soupObject.find(class_='regular').find(id='percent3').get_text()
                totalnineties = soupObject.find(class_='stat_levelcount').get_text()
                statcount = soupObject.find(class_='stat_statcount').get_text()
                nine = totalnineties.replace("'", "")
                line = xp
                if name.lower() == "azero":
                    await self.bot.say("```\n" + "Progress bar: [█████████]" + "\n" + name + " has " + "100%" + " of max cape done\n" + "23 level 99's"+ "\nand " + "0 exp remaining in 0 skills" + "```" + "\nIf this has not updated go to http://oldschool.runeclan.com/xp-tracker/user/" + s + "/stats")
                elif xp == "Max cape achieved":
                    await self.bot.say("Congratulations " + name + " on maxing!")
                else:
                    for char in ' % x p t o m a x f c e M h i v d':
                        line = line.replace(char,'')
                    percentage = float(line)
                    p = str(percentage)
                    check = len(p)
                    check2 = str(check)
                    if check <= 3:
                        if percentage <= 7.9: pbar = "Progress bar: [          ]"
                        elif percentage <= 17.0: pbar = "Progress bar: [█         ]"
                        elif percentage <= 37.0: pbar = "Progress bar: [███       ]"
                        elif percentage <= 57.0: pbar = "Progress bar: [█████     ]"
                        elif percentage <= 77.0: pbar = "Progress bar: [███████   ]"
                        elif percentage <= 100.0: pbar = "Progress bar: [█████████ ]"
                    else:
                        if percentage <= 7.9: pbar = "Progress bar: [          ]"
                        elif percentage <= 17.0: pbar = "Progress bar: [█         ]"
                        elif percentage <= 37.0: pbar = "Progress bar: [███       ]"
                        elif percentage <= 57.0: pbar = "Progress bar: [█████     ]"
                        elif percentage <= 77.0: pbar = "Progress bar: [███████   ]"
                        elif percentage <= 1000.0: pbar = "Progress bar: [█████████ ]"
                    await self.bot.say("```\n" + pbar + "\n" + name + " has " + xp + " done\n" + nine + "\nand " + statcount + "```" + "\nIf this has not updated go to http://oldschool.runeclan.com/xp-tracker/user/" + s + "/stats")
            except:
                await self.bot.say('You are currently not being tracked by Runeclan, make sure you do this at http://oldschool.runeclan.com/xp-tracker/user/' + s)

    @commands.command(no_pm=True, aliases=["EHP", "Ehp"])
    async def ehp(self, *, name):
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        async with aiohttp.get(url) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                ehp = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                splitehp = ehp.split(" ")
                splitehp[19] = splitehp[19].replace(')First', '')
                splitehp[19] = splitehp[19].replace('(', '')
                if splitehp[19] == "tracked:":
                    splitehp[19] = "inactive"
                    splitehp[18] = splitehp[18].replace('First', '')
                    embed=discord.Embed(title="", color=0x0000a0)
                    embed.set_thumbnail(url="https://crystalmathlabs.com/tracker/images/skills/ehp.gif")
                    embed.set_author(name=name)
                    embed.add_field(name="EHP:", value=splitehp[18], inline=True)
                    embed.add_field(name="Rank:", value=splitehp[19], inline=True)
                    await self.bot.say(embed=embed)
                else:
                    embed=discord.Embed(title="", color=0x0000a0)
                    embed.set_thumbnail(url="https://crystalmathlabs.com/tracker/images/skills/ehp.gif")
                    embed.set_author(name=name)
                    embed.add_field(name="EHP:", value=splitehp[18], inline=True)
                    embed.add_field(name="Rank:", value=splitehp[19], inline=True)
                    await self.bot.say(embed=embed)
            except:
                await self.bot.say("Not a valid name or not a player.")

    @commands.command(no_pm=True, aliases=["Best_Day"])
    async def best_day(self, *, name):
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        url2 = "https://crystalmathlabs.com/tracker/track.php?player={0}&skill=ehp" .format(s)
        async with aiohttp.get(url2) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                ehp2 = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                splitehp2 = ehp2.split(" ")
                splitehp2 = str(splitehp2[4])
                splitehp2 = splitehp2.replace("Week:", "")
                async with aiohttp.get(url) as response:
                    soupObject2 = BeautifulSoup(await response.text(), "html.parser")
                    ehp = soupObject2.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                    splitehp = ehp.split(" ")
                    splitehp[4] = splitehp[4].replace("Week:", "")
                    embed=discord.Embed(title="", color=0xffff00)
                    embed.set_thumbnail(url="https://crystalmathlabs.com/tracker/images/skills/ehp.gif")
                    embed.set_author(name=name)
                    embed.add_field(name="Best XP Day:", value=splitehp[4], inline=True)
                    embed.add_field(name="Best EHP Day:", value=splitehp2, inline=True)
                    await self.bot.say(embed=embed)
            except:
                await self.bot.say("Not a valid name or not a player.")

    @commands.command(no_pm=True, aliases=["Best_Week"])
    async def best_week(self, *, name):
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        url2 = "https://crystalmathlabs.com/tracker/track.php?player={0}&skill=ehp" .format(s)
        async with aiohttp.get(url2) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                ehp2 = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                splitehp2 = ehp2.split(" ")
                splitehp2 = str(splitehp2[5])
                splitehp2 = splitehp2.replace("Month:", "")
                async with aiohttp.get(url) as response:
                    soupObject2 = BeautifulSoup(await response.text(), "html.parser")
                    ehp = soupObject2.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                    splitehp = ehp.split(" ")
                    splitehp[5] = splitehp[5].replace("Month:", "")
                    embed=discord.Embed(title="", color=0x800080)
                    embed.set_thumbnail(url="https://crystalmathlabs.com/tracker/images/skills/ehp.gif")
                    embed.set_author(name=name)
                    embed.add_field(name="Best XP Week:", value=splitehp[5], inline=True)
                    embed.add_field(name="Best EHP Week:", value=splitehp2, inline=True)
                    await self.bot.say(embed=embed)
            except:
                await self.bot.say("Not a valid name or not a player.")

    @commands.command(no_pm=True, aliases=["Best_Month"])
    async def best_month(self, *, name):
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        url2 = "https://crystalmathlabs.com/tracker/track.php?player={0}&skill=ehp" .format(s)
        async with aiohttp.get(url2) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                ehp2 = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                splitehp2 = ehp2.split(" ")
                splitehp2 = str(splitehp2[6])
                splitehp2 = splitehp2.replace("Year:", "")
                async with aiohttp.get(url) as response:
                    soupObject2 = BeautifulSoup(await response.text(), "html.parser")
                    ehp = soupObject2.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                    splitehp = ehp.split(" ")
                    splitehp[6] = splitehp[6].replace("Year:", "")
                    embed=discord.Embed(title="", color=0xff80c0)
                    embed.set_thumbnail(url="https://crystalmathlabs.com/tracker/images/skills/ehp.gif")
                    embed.set_author(name=name)
                    embed.add_field(name="Best XP Month:", value=splitehp[6], inline=True)
                    embed.add_field(name="Best EHP Month:", value=splitehp2, inline=True)
                    await self.bot.say(embed=embed)
            except:
                await self.bot.say("Not a valid name or not a player.")

    @commands.command(no_pm=True, aliases=["Best_Year"])
    async def best_year(self, *, name):
        s = re.sub('\s+', '+', name)
        url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        url2 = "https://crystalmathlabs.com/tracker/track.php?player={0}&skill=ehp" .format(s)
        async with aiohttp.get(url2) as response:
            soupObject = BeautifulSoup(await response.text(), "html.parser")
            try:
                ehp2 = soupObject.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                splitehp2 = ehp2.split(" ")
                splitehp2 = str(splitehp2[7])
                splitehp2 = splitehp2.replace("\nOtherTrackStatsUpdate", "")
                async with aiohttp.get(url) as response:
                    soupObject2 = BeautifulSoup(await response.text(), "html.parser")
                    ehp = soupObject2.find(id='wrap').find(id='contentwrap').find(id='sidebar').get_text()
                    splitehp = ehp.split(" ")
                    splitehp[7] = splitehp[7].replace("\nOtherTrackStatsUpdate", "")
                    embed=discord.Embed(title="", color=0x000000)
                    embed.set_thumbnail(url="https://crystalmathlabs.com/tracker/images/skills/ehp.gif")
                    embed.set_author(name=name)
                    embed.add_field(name="Best XP Year:", value=splitehp[7], inline=True)
                    embed.add_field(name="Best EHP Year:", value=splitehp2, inline=True)
                    await self.bot.say(embed=embed)
            except:
                await self.bot.say("Not a valid name or not a player.")

    @commands.command()
    async def gtest(self, timespan, *, name):
        """day/week/month/year/total and then your name"""
        if name == None:
            name = timespan
        s = re.sub('\s+', '+', name)
        d = timespan.lower()
        url2 = "https://crystalmathlabs.com/tracker/update.php?player={0}" .format(s)
        if d != "day" and d != "week" and d != "month" and d != "year" and d != "total":
            await self.bot.say("```%gains <timespan> <name>\n \nday/week/month/year/total and then your name```")
            exit()
        if d == "day":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=1d" .format(s)
        if d == "week":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}" .format(s)
        if d == "month":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=31d" .format(s)
        if d == "year":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=365d" .format(s)
        if d == "total":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        async with aiohttp.get(url2) as response:
            sleep(0)
            async with aiohttp.get(url) as response:
                soupObject = BeautifulSoup(await response.text(), "html.parser")
                try:
                    exp = soupObject.find(id="stats_table").find_all('td')
                    embed=discord.Embed(title="", color=0xff80c0)
                    mamamia = re.split(r'[&]', url)
                    overall = str(exp[8])
                    overall = re.split(r'[><]', overall)
                    if d == "day":
                        mamamia = "&amp;time=1d"
                    if d == "week":
                        mamamia = ""
                    if d == "month":
                        mamamia = "&amp;time=31d"
                    if d == "year":
                        mamamia = "&amp;time=365d"
                    if d == "total":
                        mamamia = "$amp;time=all"
                    if overall[4] == "":
                        overall[4] = "0"
                    if str(exp[8]) == '<td class="column_skill"><img src="images/skills/overall.gif" title="Overall"/> &gt;<a href="track.php?player={0}{1}">Overall</a></td>'.format(s, mamamia) :
                        overall = str(exp[9])
                        overall = re.split(r'[><]', overall)
                        if overall[4] == "":
                            overall[4] = "0"
                        attack = str(exp[16])
                        attack = re.split(r'[><]', attack)
                        if attack[4] == "":
                            attack[4] = "0"
                        defence = str(exp[23])
                        defence = re.split(r'[><]', defence)
                        if defence[4] == "":
                            defence[4] = "0"
                        strength = str(exp[30])
                        strength = re.split(r'[><]', strength)
                        if strength[4] == "":
                            strength[4] = "0"
                        hitpoints = str(exp[37])
                        hitpoints = re.split(r'[><]', hitpoints)
                        if hitpoints[4] == "":
                            hitpoints[4] = "0"
                        ranged = str(exp[44])
                        ranged = re.split(r'[><]', ranged)
                        if ranged[4] == "":
                            ranged[4] = "0"
                        prayer = str(exp[51])
                        prayer = re.split(r'[><]', prayer)
                        if prayer[4] == "":
                            prayer[4] = "0"
                        magic = str(exp[58])
                        magic = re.split(r'[><]', magic)
                        if magic[4] == "":
                            magic[4] = "0"
                        cooking = str(exp[65])
                        cooking = re.split(r'[><]', cooking)
                        if cooking[4] == "":
                            cooking[4] = "0"
                        woodcutting = str(exp[72])
                        woodcutting = re.split(r'[><]', woodcutting)
                        if woodcutting[4] == "":
                            woodcutting[4] = "0"
                        fletching = str(exp[79])
                        fletching = re.split(r'[><]', fletching)
                        if fletching[4] == "":
                            fletching[4] = "0"
                        fishing = str(exp[86])
                        fishing = re.split(r'[><]', fishing)
                        if fishing[4] == "":
                            fishing[4] = "0"
                        firemaking = str(exp[93])
                        firemaking = re.split(r'[><]', firemaking)
                        if firemaking[4] == "":
                            firemaking[4] = "0"
                        crafting = str(exp[100])
                        crafting = re.split(r'[><]', crafting)
                        if crafting[4] == "":
                            crafting[4] = "0"
                        smithing = str(exp[107])
                        smithing = re.split(r'[><]', smithing)
                        if smithing[4] == "":
                            smithing[4] = "0"
                        mining = str(exp[114])
                        mining = re.split(r'[><]', mining)
                        if mining[4] == "":
                            mining[4] = "0"
                        herblore = str(exp[121])
                        herblore = re.split(r'[><]', herblore)
                        if herblore[4] == "":
                            herblore[4] = "0"
                        agility = str(exp[128])
                        agility = re.split(r'[><]', agility)
                        if agility[4] == "":
                            agility[4] = "0"
                        thieving = str(exp[135])
                        thieving = re.split(r'[><]', thieving)
                        if thieving[4] == "":
                            thieving[4] = "0"
                        slayer = str(exp[142])
                        slayer = re.split(r'[><]', slayer)
                        if slayer[4] == "":
                            slayer[4] = "0"
                        farming = str(exp[149])
                        farming = re.split(r'[><]', farming)
                        if farming[4] == "":
                            farming[4] = "0"
                        runecrafting = str(exp[156])
                        runecrafting = re.split(r'[><]', runecrafting)
                        if runecrafting[4] == "":
                            runecrafting[4] = "0"
                        hunter = str(exp[163])
                        hunter = re.split(r'[><]', hunter)
                        if hunter[4] == "":
                            hunter[4] = "0"
                        construction = str(exp[170])
                        construction = re.split(r'[><]', construction)
                        if construction[4] == "":
                            construction[4] = "0"
                        ehp = str(exp[177])
                        ehp = re.split(r'[><]', ehp)
                        if ehp[4] == "":
                            ehp = "0"
                        ehpim = str(exp[183])
                        ehpim = re.split(r'[><]', ehpim)
                        if ehpim[4] == "":
                            ehpim = "0"
                        embed.add_field(name='\u200b', value="<:overall:408342048798932993> " + overall[4] + "\n" + "<:attack:408350215360479234> " + attack[4] + "\n" + "<:defence:408351259238727710> " + defence[4] + "\n" + "<:strength:408351960119377959> " + strength[4]
                        + "\n" + "<:hitpoints:408352533451505664> " + hitpoints[4] + "\n" + "<:ranged:408353107689472019> " + ranged[4] + "\n" + "<:prayer:408353780330004481> " + prayer[4] + "\n" + "<:magic:408354096651698186> " + magic[4], inline=True)
                        embed.add_field(name='\u200b', value="<:cooking2:408373407378571284> " + cooking[4] + "\n" + "<:woodcutting:408374904359092245> " + woodcutting[4] + "\n" + "<:fletching:408375514869399554> " + fletching[4] + "\n" + "<:fishing:408376101061263370> "
                        + fishing[4] + "\n" + "<:firemaking:408376572035465237> " + firemaking[4] + "\n" + "<:crafting:408376899761471490> " + crafting[4] + "\n" + "<:smithing:408377374523260929> " + smithing[4] + "\n" + "<:mining:408377380806328321> " + mining[4], inline=True)
                        embed.add_field(name='\u200b', value="<:herblore:408378994438832128> " + herblore[4] + "\n" + "<:agility:408379555842228245> " + agility[4] + "\n" + "<:thieving:408379921409507357> " + thieving[4] + "\n" + "<:slayer:408380195955802123> " + slayer[4]
                        + "\n" + "<:farming:408380489704144896> " + farming[4] + "\n" + "<:runecrafting:408380867392831508> " + runecrafting[4] + "\n" + "<:hunter:408381186671378452> " + hunter[4] + "\n" + "<:construction22:408382045841129473> " + construction[4], inline=True)
                        embed.add_field(name="EHP:", value="<:ehp:408384216406163457> " + ehp[4])
                        embed.add_field(name="Special EHP", value="<:ehp_im:408406713599787008>" + ehpim[4])
                        embed.set_footer(text="Timespan: " + timespan)
                        embed.set_author(name=name)
                        await self.bot.say(embed=embed)
                        return
                    attack = str(exp[14])
                    attack = re.split(r'[><]', attack)
                    if attack[4] == "":
                        attack[4] = "0"
                    defence = str(exp[20])
                    defence = re.split(r'[><]', defence)
                    if defence[4] == "":
                        defence[4] = "0"
                    strength = str(exp[26])
                    strength = re.split(r'[><]', strength)
                    if strength[4] == "":
                        strength[4] = "0"
                    hitpoints = str(exp[32])
                    hitpoints = re.split(r'[><]', hitpoints)
                    if hitpoints[4] == "":
                        hitpoints[4] = "0"
                    ranged = str(exp[38])
                    ranged = re.split(r'[><]', ranged)
                    if ranged[4] == "":
                        ranged[4] = "0"
                    prayer = str(exp[44])
                    prayer = re.split(r'[><]', prayer)
                    if prayer[4] == "":
                        prayer[4] = "0"
                    magic = str(exp[50])
                    magic = re.split(r'[><]', magic)
                    if magic[4] == "":
                        magic[4] = "0"
                    cooking = str(exp[56])
                    cooking = re.split(r'[><]', cooking)
                    if cooking[4] == "":
                        cooking[4] = "0"
                    woodcutting = str(exp[62])
                    woodcutting = re.split(r'[><]', woodcutting)
                    if woodcutting[4] == "":
                        woodcutting[4] = "0"
                    fletching = str(exp[68])
                    fletching = re.split(r'[><]', fletching)
                    if fletching[4] == "":
                        fletching[4] = "0"
                    fishing = str(exp[74])
                    fishing = re.split(r'[><]', fishing)
                    if fishing[4] == "":
                        fishing[4] = "0"
                    firemaking = str(exp[80])
                    firemaking = re.split(r'[><]', firemaking)
                    if firemaking[4] == "":
                        firemaking[4] = "0"
                    crafting = str(exp[86])
                    crafting = re.split(r'[><]', crafting)
                    if crafting[4] == "":
                        crafting[4] = "0"
                    smithing = str(exp[92])
                    smithing = re.split(r'[><]', smithing)
                    if smithing[4] == "":
                        smithing[4] = "0"
                    mining = str(exp[98])
                    mining = re.split(r'[><]', mining)
                    if mining[4] == "":
                        mining[4] = "0"
                    herblore = str(exp[104])
                    herblore = re.split(r'[><]', herblore)
                    if herblore[4] == "":
                        herblore[4] = "0"
                    agility = str(exp[110])
                    agility = re.split(r'[><]', agility)
                    if agility[4] == "":
                        agility[4] = "0"
                    thieving = str(exp[116])
                    thieving = re.split(r'[><]', thieving)
                    if thieving[4] == "":
                        thieving[4] = "0"
                    slayer = str(exp[122])
                    slayer = re.split(r'[><]', slayer)
                    if slayer[4] == "":
                        slayer[4] = "0"
                    farming = str(exp[128])
                    farming = re.split(r'[><]', farming)
                    if farming[4] == "":
                        farming[4] = "0"
                    runecrafting = str(exp[134])
                    runecrafting = re.split(r'[><]', runecrafting)
                    if runecrafting[4] == "":
                        runecrafting[4] = "0"
                    hunter = str(exp[140])
                    hunter = re.split(r'[><]', hunter)
                    if hunter[4] == "":
                        hunter[4] = "0"
                    construction = str(exp[146])
                    construction = re.split(r'[><]', construction)
                    if construction[4] == "":
                        construction[4] = "0"
                    ehp = str(exp[152])
                    ehp = re.split(r'[><]', ehp)
                    if ehp[4] == "":
                        ehp = "noob"
                    embed.add_field(name='\u200b', value="<:overall:408342048798932993> " + overall[4] + "\n" + "<:attack:408350215360479234> " + attack[4] + "\n" + "<:defence:408351259238727710> " + defence[4] + "\n" + "<:strength:408351960119377959> " + strength[4]
                    + "\n" + "<:hitpoints:408352533451505664> " + hitpoints[4] + "\n" + "<:ranged:408353107689472019> " + ranged[4] + "\n" + "<:prayer:408353780330004481> " + prayer[4] + "\n" + "<:magic:408354096651698186> " + magic[4], inline=True)
                    embed.add_field(name='\u200b', value="<:cooking2:408373407378571284> " + cooking[4] + "\n" + "<:woodcutting:408374904359092245> " + woodcutting[4] + "\n" + "<:fletching:408375514869399554> " + fletching[4] + "\n" + "<:fishing:408376101061263370> "
                    + fishing[4] + "\n" + "<:firemaking:408376572035465237> " + firemaking[4] + "\n" + "<:crafting:408376899761471490> " + crafting[4] + "\n" + "<:smithing:408377374523260929> " + smithing[4] + "\n" + "<:mining:408377380806328321> " + mining[4], inline=True)
                    embed.add_field(name='\u200b', value="<:herblore:408378994438832128> " + herblore[4] + "\n" + "<:agility:408379555842228245> " + agility[4] + "\n" + "<:thieving:408379921409507357> " + thieving[4] + "\n" + "<:slayer:408380195955802123> " + slayer[4]
                    + "\n" + "<:farming:408380489704144896> " + farming[4] + "\n" + "<:runecrafting:408380867392831508> " + runecrafting[4] + "\n" + "<:hunter:408381186671378452> " + hunter[4] + "\n" + "<:construction22:408382045841129473> " + construction[4], inline=True)
                    embed.add_field(name="EHP:", value="<:ehp:408384216406163457> " + ehp[4])
                    embed.set_footer(text="Timespan: " + timespan)
                    embed.set_author(name=name)
                    await self.bot.say(embed=embed)
                except BaseException as e:
                    await self.bot.say(e)
    @commands.command()
    async def gains(self, timespan, *, name):
        """day/week/month/year/total and then your name"""
        if name == None:
            name = timespan
        s = re.sub('\s+', '+', name)
        s = s.lower()
        d = timespan.lower()
        url2 = "https://crystalmathlabs.com/tracker/update.php?player={0}" .format(s)
        if d != "day" and d != "week" and d != "month" and d != "year" and d != "total":
            await self.bot.say("```%gains <timespan> <name>\n \nday/week/month/year/total and then your name```")
            exit()
        if d == "day":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=1d" .format(s)
        if d == "week":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}" .format(s)
        if d == "month":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=31d" .format(s)
        if d == "year":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=365d" .format(s)
        if d == "total":
            url = "https://crystalmathlabs.com/tracker/track.php?player={0}&time=all" .format(s)
        async with aiohttp.get(url2) as response:
            sleep(0)
            async with aiohttp.get(url) as response:
                soupObject = BeautifulSoup(await response.text(), "html.parser")
                try:
                    exp = soupObject.find(id="stats_table").find_all('td')
                    embed=discord.Embed(title="", color=0xff80c0)
                    mamamia = re.split(r'[&]', url)
                    overall = str(exp[8])
                    overall = re.split(r'[><]', overall)
                    if d == "day":
                        mamamia = "&amp;time=1d"
                    if d == "week":
                        mamamia = ""
                    if d == "month":
                        mamamia = "&amp;time=31d"
                    if d == "year":
                        mamamia = "&amp;time=365d"
                    if d == "total":
                        mamamia = "&amp;time=all"
                    if overall[4] == "":
                        overall[4] = "0"
                    if str(exp[8]) == '<td class="column_skill"><img src="images/skills/overall.gif" title="Overall"/> &gt;<a href="track.php?player={0}{1}">Overall</a></td>'.format(s, mamamia) :

                        overall = str(exp[9])
                        overall = re.split(r'[><]', overall)
                        if overall[4] == "":
                            overall[4] = "0"
                        attack = str(exp[16])
                        attack = re.split(r'[><]', attack)
                        if attack[4] == "":
                            attack[4] = "0"
                        defence = str(exp[23])
                        defence = re.split(r'[><]', defence)
                        if defence[4] == "":
                            defence[4] = "0"
                        strength = str(exp[30])
                        strength = re.split(r'[><]', strength)
                        if strength[4] == "":
                            strength[4] = "0"
                        hitpoints = str(exp[37])
                        hitpoints = re.split(r'[><]', hitpoints)
                        if hitpoints[4] == "":
                            hitpoints[4] = "0"
                        ranged = str(exp[44])
                        ranged = re.split(r'[><]', ranged)
                        if ranged[4] == "":
                            ranged[4] = "0"
                        prayer = str(exp[51])
                        prayer = re.split(r'[><]', prayer)
                        if prayer[4] == "":
                            prayer[4] = "0"
                        magic = str(exp[58])
                        magic = re.split(r'[><]', magic)
                        if magic[4] == "":
                            magic[4] = "0"
                        cooking = str(exp[65])
                        cooking = re.split(r'[><]', cooking)
                        if cooking[4] == "":
                            cooking[4] = "0"
                        woodcutting = str(exp[72])
                        woodcutting = re.split(r'[><]', woodcutting)
                        if woodcutting[4] == "":
                            woodcutting[4] = "0"
                        fletching = str(exp[79])
                        fletching = re.split(r'[><]', fletching)
                        if fletching[4] == "":
                            fletching[4] = "0"
                        fishing = str(exp[86])
                        fishing = re.split(r'[><]', fishing)
                        if fishing[4] == "":
                            fishing[4] = "0"
                        firemaking = str(exp[93])
                        firemaking = re.split(r'[><]', firemaking)
                        if firemaking[4] == "":
                            firemaking[4] = "0"
                        crafting = str(exp[100])
                        crafting = re.split(r'[><]', crafting)
                        if crafting[4] == "":
                            crafting[4] = "0"
                        smithing = str(exp[107])
                        smithing = re.split(r'[><]', smithing)
                        if smithing[4] == "":
                            smithing[4] = "0"
                        mining = str(exp[114])
                        mining = re.split(r'[><]', mining)
                        if mining[4] == "":
                            mining[4] = "0"
                        herblore = str(exp[121])
                        herblore = re.split(r'[><]', herblore)
                        if herblore[4] == "":
                            herblore[4] = "0"
                        agility = str(exp[128])
                        agility = re.split(r'[><]', agility)
                        if agility[4] == "":
                            agility[4] = "0"
                        thieving = str(exp[135])
                        thieving = re.split(r'[><]', thieving)
                        if thieving[4] == "":
                            thieving[4] = "0"
                        slayer = str(exp[142])
                        slayer = re.split(r'[><]', slayer)
                        if slayer[4] == "":
                            slayer[4] = "0"
                        farming = str(exp[149])
                        farming = re.split(r'[><]', farming)
                        if farming[4] == "":
                            farming[4] = "0"
                        runecrafting = str(exp[156])
                        runecrafting = re.split(r'[><]', runecrafting)
                        if runecrafting[4] == "":
                            runecrafting[4] = "0"
                        hunter = str(exp[163])
                        hunter = re.split(r'[><]', hunter)
                        if hunter[4] == "":
                            hunter[4] = "0"
                        construction = str(exp[170])
                        construction = re.split(r'[><]', construction)
                        if construction[4] == "":
                            construction[4] = "0"
                        ehp = str(exp[177])
                        ehp = re.split(r'[><]', ehp)
                        if ehp[4] == "":
                            ehp = "0"
                        ehpim = str(exp[183])
                        ehpim = re.split(r'[><]', ehpim)
                        acctype = str(exp[182])
                        acctype = re.split(r'[><]', acctype)
                        if ehpim[4] == "":
                            ehpim = "0"
                        embed.add_field(name='\u200b', value="<:overall:408342048798932993> " + overall[4] + "\n" + "<:attack:408350215360479234> " + attack[4] + "\n" + "<:defence:408351259238727710> " + defence[4] + "\n" + "<:strength:408351960119377959> " + strength[4]
                        + "\n" + "<:hitpoints:408352533451505664> " + hitpoints[4] + "\n" + "<:ranged:408353107689472019> " + ranged[4] + "\n" + "<:prayer:408353780330004481> " + prayer[4] + "\n" + "<:magic:408354096651698186> " + magic[4], inline=True)
                        embed.add_field(name='\u200b', value="<:cooking2:408373407378571284> " + cooking[4] + "\n" + "<:woodcutting:408374904359092245> " + woodcutting[4] + "\n" + "<:fletching:408375514869399554> " + fletching[4] + "\n" + "<:fishing:408376101061263370> "
                        + fishing[4] + "\n" + "<:firemaking:408376572035465237> " + firemaking[4] + "\n" + "<:crafting:408376899761471490> " + crafting[4] + "\n" + "<:smithing:408377374523260929> " + smithing[4] + "\n" + "<:mining:408377380806328321> " + mining[4], inline=True)
                        embed.add_field(name='\u200b', value="<:herblore:408378994438832128> " + herblore[4] + "\n" + "<:agility:408379555842228245> " + agility[4] + "\n" + "<:thieving:408379921409507357> " + thieving[4] + "\n" + "<:slayer:408380195955802123> " + slayer[4]
                        + "\n" + "<:farming:408380489704144896> " + farming[4] + "\n" + "<:runecrafting:408380867392831508> " + runecrafting[4] + "\n" + "<:hunter:408381186671378452> " + hunter[4] + "\n" + "<:construction22:408382045841129473> " + construction[4], inline=True)
                        embed.add_field(name="EHP:", value="<:ehp:408384216406163457> " + ehp[4])
                        embed.add_field(name=acctype[6], value="<:ehp_im:408406713599787008>" + ehpim[4])
                        embed.set_footer(text="Timespan: " + timespan)
                        embed.set_author(name=name)
                        await self.bot.say(embed=embed)
                        return
                    attack = str(exp[14])
                    attack = re.split(r'[><]', attack)
                    if attack[4] == "":
                        attack[4] = "0"
                    defence = str(exp[20])
                    defence = re.split(r'[><]', defence)
                    if defence[4] == "":
                        defence[4] = "0"
                    strength = str(exp[26])
                    strength = re.split(r'[><]', strength)
                    if strength[4] == "":
                        strength[4] = "0"
                    hitpoints = str(exp[32])
                    hitpoints = re.split(r'[><]', hitpoints)
                    if hitpoints[4] == "":
                        hitpoints[4] = "0"
                    ranged = str(exp[38])
                    ranged = re.split(r'[><]', ranged)
                    if ranged[4] == "":
                        ranged[4] = "0"
                    prayer = str(exp[44])
                    prayer = re.split(r'[><]', prayer)
                    if prayer[4] == "":
                        prayer[4] = "0"
                    magic = str(exp[50])
                    magic = re.split(r'[><]', magic)
                    if magic[4] == "":
                        magic[4] = "0"
                    cooking = str(exp[56])
                    cooking = re.split(r'[><]', cooking)
                    if cooking[4] == "":
                        cooking[4] = "0"
                    woodcutting = str(exp[62])
                    woodcutting = re.split(r'[><]', woodcutting)
                    if woodcutting[4] == "":
                        woodcutting[4] = "0"
                    fletching = str(exp[68])
                    fletching = re.split(r'[><]', fletching)
                    if fletching[4] == "":
                        fletching[4] = "0"
                    fishing = str(exp[74])
                    fishing = re.split(r'[><]', fishing)
                    if fishing[4] == "":
                        fishing[4] = "0"
                    firemaking = str(exp[80])
                    firemaking = re.split(r'[><]', firemaking)
                    if firemaking[4] == "":
                        firemaking[4] = "0"
                    crafting = str(exp[86])
                    crafting = re.split(r'[><]', crafting)
                    if crafting[4] == "":
                        crafting[4] = "0"
                    smithing = str(exp[92])
                    smithing = re.split(r'[><]', smithing)
                    if smithing[4] == "":
                        smithing[4] = "0"
                    mining = str(exp[98])
                    mining = re.split(r'[><]', mining)
                    if mining[4] == "":
                        mining[4] = "0"
                    herblore = str(exp[104])
                    herblore = re.split(r'[><]', herblore)
                    if herblore[4] == "":
                        herblore[4] = "0"
                    agility = str(exp[110])
                    agility = re.split(r'[><]', agility)
                    if agility[4] == "":
                        agility[4] = "0"
                    thieving = str(exp[116])
                    thieving = re.split(r'[><]', thieving)
                    if thieving[4] == "":
                        thieving[4] = "0"
                    slayer = str(exp[122])
                    slayer = re.split(r'[><]', slayer)
                    if slayer[4] == "":
                        slayer[4] = "0"
                    farming = str(exp[128])
                    farming = re.split(r'[><]', farming)
                    if farming[4] == "":
                        farming[4] = "0"
                    runecrafting = str(exp[134])
                    runecrafting = re.split(r'[><]', runecrafting)
                    if runecrafting[4] == "":
                        runecrafting[4] = "0"
                    hunter = str(exp[140])
                    hunter = re.split(r'[><]', hunter)
                    if hunter[4] == "":
                        hunter[4] = "0"
                    construction = str(exp[146])
                    construction = re.split(r'[><]', construction)
                    if construction[4] == "":
                        construction[4] = "0"
                    ehp = str(exp[152])
                    ehp = re.split(r'[><]', ehp)
                    if ehp[4] == "":
                        ehp = "noob"
                    embed.add_field(name='\u200b', value="<:overall:408342048798932993> " + overall[4] + "\n" + "<:attack:408350215360479234> " + attack[4] + "\n" + "<:defence:408351259238727710> " + defence[4] + "\n" + "<:strength:408351960119377959> " + strength[4]
                    + "\n" + "<:hitpoints:408352533451505664> " + hitpoints[4] + "\n" + "<:ranged:408353107689472019> " + ranged[4] + "\n" + "<:prayer:408353780330004481> " + prayer[4] + "\n" + "<:magic:408354096651698186> " + magic[4], inline=True)
                    embed.add_field(name='\u200b', value="<:cooking2:408373407378571284> " + cooking[4] + "\n" + "<:woodcutting:408374904359092245> " + woodcutting[4] + "\n" + "<:fletching:408375514869399554> " + fletching[4] + "\n" + "<:fishing:408376101061263370> "
                    + fishing[4] + "\n" + "<:firemaking:408376572035465237> " + firemaking[4] + "\n" + "<:crafting:408376899761471490> " + crafting[4] + "\n" + "<:smithing:408377374523260929> " + smithing[4] + "\n" + "<:mining:408377380806328321> " + mining[4], inline=True)
                    embed.add_field(name='\u200b', value="<:herblore:408378994438832128> " + herblore[4] + "\n" + "<:agility:408379555842228245> " + agility[4] + "\n" + "<:thieving:408379921409507357> " + thieving[4] + "\n" + "<:slayer:408380195955802123> " + slayer[4]
                    + "\n" + "<:farming:408380489704144896> " + farming[4] + "\n" + "<:runecrafting:408380867392831508> " + runecrafting[4] + "\n" + "<:hunter:408381186671378452> " + hunter[4] + "\n" + "<:construction22:408382045841129473> " + construction[4], inline=True)
                    embed.add_field(name="EHP:", value="<:ehp:408384216406163457> " + ehp[4])
                    embed.set_footer(text="Timespan: " + timespan)
                    embed.set_author(name=name)
                    await self.bot.say(embed=embed)
                except:
                    await self.bot.say("```name not found or not tracked on CML \nOR you haven't updated CML.```")





    @commands.command(pass_context=True, no_pm=True, aliases=["07wsid", "07Wsid", "WhatToDo", "imbored", "Whatdo", "WhatDo", "wsid", "Wsid"])
    async def whatdo(self, ctx):
        if ctx.message.author.mention == "<@!168397143533420544>":
            await self.bot.say(ctx.message.author.mention + " You should **Train Firemaking**")
        else:
            await self.bot.say(ctx.message.author.mention + " " + randchoice(self.wsids))




def check_folders():
        folders = ("data", "data/rscog/")
        for folder in folders:
            if not os.path.exists(folder):
                print("Creating " + folder + " folder...")
                os.makedirs(folder)


def check_files():
        """Moves the file from cogs to the data directory. Important -> Also changes the name to rscog.json"""
        wsids = {"reeeeeeee"}

        if not os.path.isfile("data/rscog/wsid.json"):
            if os.path.isfile("cogs/put_in_cogs_folder.json"):
                print("moving default wsid.json...")
                os.rename("cogs/put_in_cogs_folder.json", "data/rscog/wsid.json")
            else:
                print("creating default wsid.json...")
                fileIO("data/rscog/wsid.json", "save", wsids)

def setup(bot):
    check_folders()
    check_files()
    n = rscog(bot)
    bot.add_cog(n)

def setup(bot):
    if soupAvailable:
        bot.add_cog(ACZEROrsmodule(bot))
    else:
        raise RuntimeError("You need to run `pip3 install beautifulsoup4`")
